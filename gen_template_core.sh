SOURCE_PATH=$1

if [ ! $# -eq 1 ]
  then
    echo "Usage: $0 <OpenRadio source>"
    exit 1
fi

if [ ! -d "$SOURCE_PATH" ];
then
    echo "Source Path doesn't exist : $SOURCE_PATH"
    exit 1
fi

mkdir -p $SOURCE_PATH/locales/templates/core
pygettext3 -o $SOURCE_PATH/locales/templates/core/core.pot $SOURCE_PATH/core/
