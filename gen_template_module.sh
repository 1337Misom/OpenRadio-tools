SOURCE_PATH=$1
MODULE_DOMAIN=$2
MODULE_DIR_NAME=$3

if [ ! $# -eq 3 ]
  then
    echo "Usage: $0 <OpenRadio source> <Module Domain> <Module Directory Name>"
    exit 1
fi

if [ ! -d "$SOURCE_PATH" ];
then
    echo "Source Path doesn't exist : $SOURCE_PATH"
    exit 1
fi

if [ ! -d "$SOURCE_PATH/modules/$MODULE_DIR_NAME/" ];
then
    echo "Module Directory doesn't exist : $SOURCE_PATH/modules/$MODULE_DIR_NAME/"
    exit 1
fi

mkdir -p $SOURCE_PATH/locales/templates/modules/$MODULE_DOMAIN/
pygettext3 -o $SOURCE_PATH/locales/templates/modules/$MODULE_DOMAIN/$MODULE_DOMAIN.pot $SOURCE_PATH/modules/$MODULE_DIR_NAME/*
