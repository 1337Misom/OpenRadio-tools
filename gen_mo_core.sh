SOURCE_PATH=$1
LANGUAGE=$2

if [ ! $# -eq 2 ]
  then
    echo "Usage: $0 <OpenRadio source> <Language (ISO 639-1)>"
    exit 1
fi

if [ ! -d "$SOURCE_PATH" ];
then
    echo "Source Path doesn't exist : $SOURCE_PATH"
    exit 1
fi

mkdir -p $SOURCE_PATH/locales/core/$LANGUAGE/LC_MESSAGES/
msgfmt -o $SOURCE_PATH/locales/core/$LANGUAGE/LC_MESSAGES/core.mo $SOURCE_PATH/locales/src/$LANGUAGE/core/core.pot
