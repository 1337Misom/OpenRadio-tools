SOURCE_PATH=$1
MODULE_DOMAIN=$2
LANGUAGE=$3

if [ ! $# -eq 3 ]
  then
    echo "Usage: $0 <OpenRadio source> <Module Domain> <Language (ISO 639-1)>"
    exit 1
fi

if [ ! -d "$SOURCE_PATH" ];
then
    echo "Source Path doesn't exist : $SOURCE_PATH"
    exit 1
fi
mkdir -p $SOURCE_PATH/locales/modules/$MODULE_DOMAIN/$LANGUAGE/LC_MESSAGES/
msgfmt -o $SOURCE_PATH/locales/modules/$MODULE_DOMAIN/$LANGUAGE/LC_MESSAGES/$MODULE_DOMAIN.mo $SOURCE_PATH/locales/src/$LANGUAGE/modules/$MODULE_DOMAIN/$MODULE_DOMAIN.pot
