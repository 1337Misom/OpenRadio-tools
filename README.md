# OpenRadio tools
## RPI Install
1. Download and Install Raspberry Pi OS
2. Download rpi_installer.sh
3. Right click on rpi_installer.sh and set the Permission `Execute` to Anyone
4. Double Click rpi_installer.sh press `Execute` and follow the instructions. Don't press OK on Text View prompts they will close on their own.
5. Enjoy
## Localizing a module
### Be Careful and backup OpenRadio before you do anything. The scripts don't have any error checking
1. Generate the template pot for the module : `sh gen_template_module.sh <OpenRadio source> <Module Domain> <Module Directory Name>`
  * OpenRadio source = The location of the OpenRadio source code. Be careful it must be the subdirectory containing `core`, `modules`, `assets`, `locales` e.g. if you cloned it into your parent directory `../OpenRadio/OpenRadio/`
  * Module Domain = The Domain of the module e.g. `builtin.about`
  * Module Directory Name = The directory name of the module e.g. `About`

2. Edit the files located in `<the source path used in 1>/locales/templates/modules/<Domain of the module>/<Domain of the module>.pot`
3. Copy the directory `<the source path used in 1>/locales/templates/modules/<Domain of the module>/` to `<the source path specified in 1>/locales/src/<the Language for the translation (e.g. 'en')>/modules/<Domain of the module>`
4. Generate the mo files for the module. `sh gen_mo_module.sh <OpenRadio source> <Module Domain> <Language (ISO 639-1)>`
 * OpenRadio source = same as in 1
 * Module Domain = same as in 1
 * Language = The language of the translation (after ISO 639-1)
