#!/bin/bash
on_error() {
	zenity --error --text="$@"
	exit 1
}

check_internet() {
	if ! $(curl -s https://python.org) ; then
		on_error "No Internet. Verify connection"
	fi
}

check_internet

PASSWD="$(zenity --password --title=Authentication)\n"
if [ "$PASSWD" = "\n" ] ; then
	on_error "You must provide a password to continue"
fi

apt_install() {
	zenity --info --text="Installing $1"

	INSTALL_LOG=$(mktemp)
	tail +1f $INSTALL_LOG | zenity --text-info --auto-scroll &
	PID=$!
	if ! $(echo -e $PASSWD | sudo -S apt install -y $1 > $INSTALL_LOG) ; then
       		kill $PID
		on_error "Unable to install dependencies. Log can be found at $INSTALL_LOG"
	fi
	rm $INSTALL_LOG
	kill $PID
}

dab_install() {
	apt_install "libfaad-dev libfftw3-dev libusb-1.0-0-dev libsamplerate0-dev librtlsdr-dev g++ make gcc"

    cd $(mktemp -d)

    git clone --recursive https://gitlab.com/1337Misom/simple_dab_lib.git

    cd simple_dab_lib

    INSTALL_LOG=$(mktemp)

    zenity --info --text="Building and installing simple_dab_lib. This may take a while"
	tail +1f $INSTALL_LOG | zenity --text-info --auto-scroll &
        PID=$!
	if ! $(echo -e $PASSWD | sudo -S make -j$(expr $(nproc) - 1) install > $INSTALL_LOG) ; then
        	kill $PID
	        on_error "Unable to build simple_dab_lib. Log can be found at $INSTALL_LOG"
        fi
	kill $PID
}

install_dep(){
	apt_install "libgtk-3-dev portaudio19-dev libpython3-dev pkg-config libboost-all-dev vlc python3 libvlc-dev"
}


install_dep

if ! stat /usr/local/lib/libsimple_dab.so ; then

	if $(zenity --question --text="Install DAB?") ; then
		dab_install
	fi
fi

zenity --info --text="Installing OpenRadio"

INSTALL_LOG=$(mktemp)
tail +1f $INSTALL_LOG | zenity --text-info --auto-scroll &
PID=$!
if ! $(python3 -m pip install --break-system-packages OpenRadio > $INSTALL_LOG) ; then
	kill $PID
	on_error "Unable to install OpenRadio. Log can be found in $INSTALL_LOG"
fi
kill $PID

zenity --info --text="Creating desktop file"

PYTHON_SCRIPT_LOC=$(python -c 'import os,sysconfig;print(sysconfig.get_path("scripts",f"{os.name}_user"))')
PYTHON_MODULE_LOC=$(python3 -m pip show OpenRadio | grep -oP '(?<=Location: ).*')

echo "[Desktop Entry]" > ~/Desktop/OpenRadio.desktop
echo "Encoding=UTF-8" >> ~/Desktop/OpenRadio.desktop
echo "Version=1.0" >> ~/Desktop/OpenRadio.desktop 
echo "Type=Application" >> ~/Desktop/OpenRadio.desktop 
echo "Terminal=false" >> ~/Desktop/OpenRadio.desktop 
echo "Name=OpenRadio" >> ~/Desktop/OpenRadio.desktop 
echo "Category=Utility" >> ~/Desktop/OpenRadio.desktop 
echo "Exec=bash -c 'GTK_THEME=Adwaita:dark $PYTHON_SCRIPT_LOC/openradio'" >> ~/Desktop/OpenRadio.desktop
echo "Icon=$PYTHON_MODULE_LOC/OpenRadio/assets/OpenRadio.icon" >> ~/Desktop/OpenRadio.desktop
chmod +x ~/Desktop/OpenRadio.desktop

cp ~/Desktop/OpenRadio.desktop ~/.local/share/applications/OpenRadio.desktop

if zenity --question --text="AutoLaunch Openradio?" ; then
	mkdir -p ~/.config/autostart
	cp ~/Desktop/OpenRadio.desktop ~/.config/autostart/OpenRadio.desktop
fi

if zenity --question --text="Install successful. Start OpenRadio Now?" ; then
	GTK_THEME=Adwaita:dark $PYTHON_SCRIPT_LOC/openradio
fi
